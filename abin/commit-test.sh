#! /bin/bash
set -e

npm run --silent lint
npm run --silent test
